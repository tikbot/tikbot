# TikBot

This repository contains the deployment files for the [TikBot project](https://gitlab.com/tikbot).

## Deployment

Adjust neccesary variables in `.env`.

Deploy via `docker-compose`.
